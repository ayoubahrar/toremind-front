import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable } from 'rxjs';
import { Cartographie } from '../services/cartographie.model';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export class SampleObject{
   date : string;
	 identifiant : string;
	 chantier : string ;
}

@Injectable({
  providedIn: 'root'
})
export class CartographieService {
  private savedUrl ='/cartographies/saved';
  private finishedUrl ='/cartographies/finished';


  constructor(private http:HttpClient) { }

  public getCartographieSaved():Observable<Cartographie[]> {
    return this.http.get<Cartographie[]>(this.savedUrl);
  }
  public getCartographiesFinished() :Observable<Cartographie[]> {
    return this.http.get<Cartographie[]>(this.finishedUrl);
  }

}

