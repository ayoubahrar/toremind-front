import { TestBed } from '@angular/core/testing';

import { CartographieService } from './cartographie.service';

describe('CartographieService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CartographieService = TestBed.get(CartographieService);
    expect(service).toBeTruthy();
  });
});
