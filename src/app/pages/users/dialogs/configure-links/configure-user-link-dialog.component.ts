import { Component } from '@angular/core';

@Component({
    selector: 'configure-user-link-dialog',
    templateUrl: 'configure-user-link-dialog.html',
  })
  export class ConfigureUserLinkDialog {}