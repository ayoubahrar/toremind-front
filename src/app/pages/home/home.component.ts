import { Component,OnInit, AfterViewInit, ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';
import {merge, Observable} from 'rxjs';  
import {MatPaginator, MatTableDataSource, MatDialog,MatSort} from '@angular/material';
import { RenameGroupsDialog } from './dialogs/rename-groups/rename-groups.component';

import { CartographieService } from '../../services/cartographie.service';
import { Cartographie } from '../../services/cartographie.model' ;
// import { ModifyAdminDescriptionDialog } from '../admins/dialogs/modify-description/modify-description-dialog.component';

export interface Options {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{
  displayedColumns: string[] = ['createdDate','email','chantier', 'actions'];
  dsCartosSauve : MatTableDataSource<Cartographie>;
  dsCartosDone : MatTableDataSource<Cartographie>;
  searchMode: string = 'admins';
  groupeType: string = 'mine';
 
  @ViewChild('t1', {read: MatSort})sort: MatSort; 
  @ViewChild('paginator1', {read: MatPaginator})  paginator: MatPaginator;
  @ViewChild('t2', {read: MatSort})  sortDone: MatSort;
  @ViewChild('paginator2', {read: MatPaginator})  paginatorDone: MatPaginator;


  constructor(
      //public dialog: ModifyAdminDescriptionDialog,
      private cartographieService: CartographieService) {}


  myControl = new FormControl();
  users: string[] = ['Zakaria DRISSI', 'Xavier HUMEAU', 'John Doe'];
  filteredUsers: Observable<string[]>;

 

  ngOnInit() {

    this.cartographieService.getCartographiesFinished().subscribe( 
      (data : Cartographie[]) => {
        this.dsCartosDone = new MatTableDataSource<Cartographie>(data);
        this.dsCartosDone.paginator = this.paginatorDone; 
        this.dsCartosDone.sort = this.sortDone;
      },
      error => console.log("Error :: " + error)   
    );
     

    this.cartographieService.getCartographieSaved().subscribe( 
      (data : Cartographie[]) => {
        this.dsCartosSauve = new MatTableDataSource<Cartographie>(data);
        this.dsCartosSauve.paginator = this.paginator;
        this.dsCartosSauve.sort = this.sort;        
      },
      error => console.log("Error :: " + error)   
    );
  }

  filterCartoSauve(filterValue: string,field : string) {
    this.dsCartosSauve.filterPredicate = (data: Cartographie, filter: string) => {
      if(field==='date')
      return data.createdDate.indexOf(filter) != -1;
      if(field==='email')
      return data.email.trim().toLowerCase().indexOf(filter) != -1;
      if(field==='chantier')
      return data.codeChantier.trim().toLowerCase().indexOf(filter) != -1;
    };
    this.dsCartosSauve.filter = filterValue.trim().toLowerCase();
  }

  filterCartoFinal(filterValue: string,field : string) {
    this.dsCartosDone.filterPredicate = (data: Cartographie, filter: string) => {
      if(field==='date')
      return data.createdDate.indexOf(filter) != -1;
      if(field==='email')
      return data.email.trim().toLowerCase().indexOf(filter) != -1;
      if(field==='chantier')
      return data.codeChantier.trim().toLowerCase().indexOf(filter) != -1;
    };
    this.dsCartosDone.filter = filterValue.trim().toLowerCase();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.users.filter(option => option.toLowerCase().includes(filterValue));
  }

  selectSearchMode(mode: string) {
    this.searchMode = mode;
  }

  selectGroupeType(type: string) {
    this.groupeType = type;
  }

  openDialog() {
  }
}

