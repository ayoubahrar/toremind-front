import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators,FormGroup,FormBuilder} from '@angular/forms';
import {FormService} from '../form.service';
import {ErrorStateMatcher} from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export interface email {
  value: string;
  viewValue: string;
}
export interface codeChantier {
  value: string;
  viewValue: string;
}
export interface entite {
  value: string;
  viewValue: string;
}
export interface uo {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.css', './../process.component.css'
  ],
})
export class IdentificationComponent {
  
  frmStepOne: FormGroup;
  
  isLinear=true;

  matcher = new MyErrorStateMatcher();

  entities: entite[] = [
    {value: 'BYBAT', viewValue: 'BYBAT'},
    {value: 'BYTELEC', viewValue: 'BYTELEC'},
    {value: 'BYCOL', viewValue: 'BYCOL'}
  ];

  uos: uo[] = [
    {value: 'REP', viewValue: 'REP'},
    {value: 'Chantier', viewValue: 'Chantier'}
  ];
  
  
  constructor(private formBuilder: FormBuilder,
    public formService: FormService) {
      this.frmStepOne = this.formBuilder.group({

        emailCtrl: ['', Validators.required],
        uoCtrl: ['', Validators.required] ,
        entityCtrl : ['', Validators.required],
        codeChantierCtrl: ['']
  
      }); 
      this.formService.stepReady(this.frmStepOne, 'one')
    }

  ngOnInit() {
    
    }
    
    
}
