import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {FormBuilder, Validators,FormControl} from '@angular/forms';
import {FormService} from '../form.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css','./../process.component.css']
  
})
export class InformationComponent implements OnInit {

 
  frmStepTwo: FormGroup;
  isLinear=true;

  constructor(private _formBuilder: FormBuilder,
    public formService: FormService) {
        this.frmStepTwo = this._formBuilder.group({
            // Controls here
        });
        this.formService.stepReady(this.frmStepTwo,'two')
     }

  ngOnInit() {
    
  }

}
