import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { FormService } from '../form.service';
@Component({
  selector: 'app-workbench',
  templateUrl: './workbench.component.html',
  styleUrls: ['./workbench.component.css','./../process.component.css']
})
export class WorkbenchComponent implements OnInit {

  frmStepThree: FormGroup;
  
  isLinear=true;
  
  constructor(private _formBuilder: FormBuilder,
    public formService: FormService) {
        this.frmStepThree = this._formBuilder.group({
          //Controls here
        });
        this.formService.stepReady(this.frmStepThree, 'three')
    }

  ngOnInit() {}
    
}

