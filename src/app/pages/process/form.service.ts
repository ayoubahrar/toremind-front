import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  private frmStepOneSource: Subject<FormGroup> = new Subject();
  frmStepOne: Observable<FormGroup> = this.frmStepOneSource.asObservable();

  private stepTwoSource: Subject<FormGroup> = new Subject();
  stepTwo: Observable<FormGroup> = this.stepTwoSource.asObservable();

  private stepThreeSource: Subject<FormGroup> = new Subject();
  stepThree: Observable<FormGroup> = this.stepTwoSource.asObservable();

  mainForm: FormGroup = this._formBuilder.group({
    id:'',
    email:'',
    entity:'',
    codeChantier:'',
    sourceSystem:'',
    planClassementInit:'',
    lastPath:'',
    status:'',
    association:'',
    uo:'',
    createdDate:'',
  })
  constructor(private _formBuilder: FormBuilder) {
    this.frmStepOne.subscribe(form =>
      form.valueChanges.subscribe(val => {
        this.mainForm.value.email = val.emailCtrl
        this.mainForm.value.entity = val.entityCtrl
        this.mainForm.value.codeChantier = val.codeChantierCtrl
        this.mainForm.value.uo = val.uoCtrl
      })
    )
    this.stepTwo.subscribe(form =>
      form.valueChanges.subscribe(val => {

      })
    )
    this.stepThree.subscribe(form =>
      form.valueChanges.subscribe(val => {
        
      })
    )
   }
   
   stepReady(form: FormGroup, part) {
    switch (part) {
      case 'one': { this.frmStepOneSource.next(form) }
      case 'two': { this.stepTwoSource.next(form) }
      case 'three': { this.stepThreeSource.next(form)}
    }
  }
}
