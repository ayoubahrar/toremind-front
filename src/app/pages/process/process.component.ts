import { Component,ViewChild, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators,FormControl} from '@angular/forms';
import {Identification} from './identification/identification.model';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';

import { FormService } from './form.service';
import { IdentificationComponent } from './identification/identification.component';
import { InformationComponent } from './information/information.component';
import { WorkbenchComponent } from './workbench/workbench.component';
@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css','./process.component.scss'],
  providers: [ FormService , {
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  } ]
})
export class ProcessComponent implements OnInit{

  
  myForm: Array<string>
  
  isLinear = true;

  frmStepOne : FormGroup;
  frmStepTwo : FormGroup;
  frmStepThree : FormGroup;

  @ViewChild(IdentificationComponent) firstFromGroup: IdentificationComponent ;
  @ViewChild(InformationComponent) secondFromGroup: InformationComponent ;
  @ViewChild(WorkbenchComponent) thirdFromGroup: WorkbenchComponent ;
  

  constructor(public formService: FormService,
    private fb: FormBuilder) { 
      this.myForm = this.formService.mainForm.value

    }

    get stepOne() {
      return this.firstFromGroup ? this.firstFromGroup.frmStepOne : null;
    }

    get stepTwo() {
      return this.secondFromGroup ? this.secondFromGroup.frmStepTwo : null;
    }

    get stepThree() {
      return this.thirdFromGroup ? this.thirdFromGroup.frmStepThree : null;
    }

    keys() : Array<string> {
      return Object.keys(this.myForm);
    }

    ngOnInit(){
     
      this.frmStepOne=this.stepOne;
      this.frmStepTwo=this.stepTwo;
      this.frmStepThree=this.stepThree;
    }
}
