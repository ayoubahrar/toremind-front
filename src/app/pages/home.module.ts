import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HomeRoutes } from './home.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { RenameGroupsDialog } from './home/dialogs/rename-groups/rename-groups.component';
import { AdminsComponent } from './admins/admins.component';
import { DeleteAdminDialog } from './admins/dialogs/delete/delete-admin-dialog.component';
import { ModifyAdminDescriptionDialog } from './admins/dialogs/modify-description/modify-description-dialog.component';
import { NewAdminComponent } from './admins/new-admin/new-admin.component';
import { UsersComponent } from './users/users.component';
import { NewUserComponent } from './users/new-user/new-user.component';
import { DeleteUserDialog } from './users/dialogs/delete/delete-user-dialog.component';
import { ModifyUserDescriptionDialog } from './users/dialogs/modify-description/modify-description-dialog.component';
import { ImportAdminComponent } from './admins/import-list/import-list.component';
import { ConfigureAdminLinkDialog } from './admins/dialogs/configure-links/configure-admin-link-dialog.component';
import { ConfigureUserLinkDialog } from './users/dialogs/configure-links/configure-user-link-dialog.component';
import { ProcessComponent } from './process/process.component';
import { IdentificationComponent } from './process/identification/identification.component';
import { InformationComponent } from './process/information/information.component';
import { WorkbenchComponent } from './process/workbench/workbench.component';


const DIALOGS: any[] = [RenameGroupsDialog, DeleteAdminDialog, ModifyAdminDescriptionDialog, 
                          DeleteUserDialog, ModifyUserDescriptionDialog, ConfigureAdminLinkDialog, ConfigureUserLinkDialog];

@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild(HomeRoutes)
  ],
  entryComponents: [...DIALOGS],
  declarations: [
    HomeComponent,
    AdminsComponent,
    NewAdminComponent,
    ImportAdminComponent,
    UsersComponent,
    NewUserComponent,    
    ...DIALOGS, ProcessComponent, IdentificationComponent, InformationComponent, WorkbenchComponent,
  ]
})
export class HomeModule { }
